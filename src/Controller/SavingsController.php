<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Saving;
use App\Repository\CustomerRepository;
use App\Repository\SavingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Uuid;

class SavingsController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager, private SavingRepository $savingRepository)
    {
    }

    #[Route('/store_savings_result', name: 'store_savings_result')]
    public function store_savings_result(?Request $request): Response
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');

        $saving = new Saving();
        $saving->setTimestamp(new \DateTime());
        $saving->setData(json_encode($_POST));

        $this->entityManager->persist($saving);
        $this->entityManager->flush();

        return $this->json([
            'id' => $saving->getId()
        ]);
    }

    #[Route('/store_email_for_savings_result', name: 'store_email_for_savings_result')]
    public function store_email_for_savings_result(?Request $request): Response
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');

        $lastInsertId = $_POST['lastInsertId'];
        $email = $_POST['email'];

        $saving = $this->savingRepository->find($lastInsertId);
        $saving->setEmail($email);

        $this->entityManager->persist($saving);
        $this->entityManager->flush();

        return $this->json([
            'id' => $saving->getId()
        ]);
    }

//    private function dropDb()
//    {
//        $application = new Application($this->kernel);
//        $application->setAutoExit(false);
//        $input = new ArrayInput([
//            'command' => 'doctrine:database:drop --force'
//        ]);
//        $output = new NullOutput();
//        $application->run($input, $output);
//    }
//
//    private function createDb()
//    {
//        $application = new Application($this->kernel);
//        $application->setAutoExit(false);
//        $input = new ArrayInput([
//            'command' => 'doctrine:database:create'
//        ]);
//        $output = new NullOutput();
//        $application->run($input, $output);
//    }
//
//    private function updateSchema()
//    {
//        $application = new Application($this->kernel);
//        $application->setAutoExit(false);
//        $input = new ArrayInput([
//            'command' => 'doctrine:schema:update'
//        ]);
//        $output = new NullOutput();
//        $application->run($input, $output);
//    }
}
