<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Uuid;

class PartnerController extends AbstractController
{
    public function __construct(private Filesystem $filesystem, private EntityManagerInterface $entityManager, private CustomerRepository $customerRepository)
    {
    }

    #[Route('/update_partner_step/{partnerId}/{newStep}', name: 'update_partner_step')]
    public function updatePartnerStep(?Request $request, int $partnerId, int $newStep): Response
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');

        $partner = $this->customerRepository->find($partnerId);
        $partner->setPartnerStep($newStep);

        $partner->setLastAction(new \DateTime());

        $this->entityManager->persist($partner);
        $this->entityManager->flush();

        return $this->json([]);
    }

    #[Route('/get_partner_list', name: 'get_partner_list')]
    public function getPartnerList(?Request $request): Response
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');

        $allPartners = $this->customerRepository->findAll();
        return $this->json((new ArrayCollection($allPartners))->map(static function(Customer $partner) {
            return [
                "id" => $partner->getId(),
                "partner_name" => $partner->getCompany(),
                "partner_contact" => $partner->getCity(),
                "location" => "–",
                "data_quality" => $partner->getDataQuality(),
                "partner_step" => $partner->getPartnerStep(),
                "last_action" => $partner->getLastAction()?->format('d.m.Y') ?? '01.01.2022',
            ];
        })->toArray());
    }

    #[Route('/upload_partner_list', name: 'upload_partner_list')]
    public function uploadPartnerList(Request $request): Response
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');

        $randomFileName = '/tmp/' . Uuid::V4_RANDOM . '.tmp';
        $this->filesystem->touch($randomFileName);
        $this->filesystem->dumpFile($randomFileName, rawurldecode($_POST['data']));

        $csvArray = array_map('str_getcsv', file($randomFileName));

        $csvArray = new ArrayCollection($csvArray);
        $csvArray = $csvArray->map(static function($csvString) {
            return explode(';', $csvString[0]);
        });

        // Create or update (when number already exists) each customer in the csv list
        foreach ($csvArray as $i => $customerData) {
            if(0 === $i) {
                continue; // skip first entry (description fields)
            }
            $customer = $this->customerRepository->findOneBy(['number' => $customerData[0]]) ?? new Customer();

            $customer->setNumber($customerData[0]);
            $customer->setCompany($customerData[1]);
            $customer->setCity($customerData[2]);
            $customer->setPostalCode($customerData[3]);
            $customer->setStreet($customerData[4]);
            $customer->setEmail($customerData[5]);
            $customer->setPartnerStep(1);
            //$customer->setDataQuality(rand(50, 100)/100);
            $customer->setDataQuality(0);

            $this->entityManager->persist($customer);
        }

        $this->entityManager->flush();

        return ($this->getPartnerList(null));
    }
//
//    /**
//     * Take uploaded orders and check them for eligibility.
//     *
//     * Copied from the wordpress website functions.php
//     */
//    private function check_orders(array $xmlContents): Response|array|string
//    {
//
//        //$files = $_FILES['orders'];
//
//        define('MULTIPART_BOUNDARY', '--------------------------'.microtime(true));
//
//        $header = 'Content-Type: multipart/form-data; boundary='.MULTIPART_BOUNDARY;
//
//        $validFiles = [];
//        $content = '';
//        $i = 0;
//        foreach ($xmlContents as $name => $_content) {
//
//        //for ($i = 0; $i < count($xmlContents); $i++) {
//
//            $fileName = 'uploaded_file_' . $i;
//            $contentType = 'text/xml';
//            $content .= "--" . MULTIPART_BOUNDARY . "\r\n". "Content-Disposition: form-data; name=\"" . $fileName . "\"; filename=\"" . $name . "\"\r\n" . "Content-Type: " . $contentType . "\r\n\r\n" . $_content . "\r\n";
//
//            $i++;
//        }
//
//        // add some POST fields to the request too: $_POST['foo'] = 'bar'
//        /*
//        $content .= "--".MULTIPART_BOUNDARY."\r\n".
//                    "Content-Disposition: form-data; name=\"foo\"\r\n\r\n".
//                    "bar\r\n";
//        */
//
//        // signal end of request (note the trailing "--")
//        $content .= "--".MULTIPART_BOUNDARY."--\r\n";
//
//        $context = stream_context_create([
//            'http' => [
//                'method' => 'POST',
//                'header' => $header,
//                'content' => $content,
//            ],
//            'ssl' => [
//                'verify_peer' => false,
//                'verify_peer_name' => false,
//            ]
//        ]);
//
//
//        //https://bb3380080e.app.hf-solutions.co/login
//        //$url = 'https://web-test.app.hf-solutions.co/connector/rating/documents/de/e222cad7-e61e-4aca-92a1-4d17d8225adc?_locale=de';
//        $url = 'https://bb3380080e.app.hf-solutions.co/connector/rating/documents/de?_locale=de&apiKey=e222cad7-e61e-4aca-92a1-4d17d8225adc';
//
//        // https://bb3380080e.app.hf-solutions.co/connector/rating/identify/{compositeId}/{creatorMail}
//        // /connector/rating/identify/{compositeId}/{creatorMail}
//        try {
//            $content = file_get_contents($url, false, $context);
//            return json_encode(json_decode($content));
//        } catch (\Exception $e) {
//            return [
//                'content' => json_encode($context),
//                'error' => $e->getMessage()
//            ];
//        }
//    }
//
//    function base64_to_jpeg($base64_string, $output_file) {
//        // open the output file for writing
//        $ifp = fopen( $output_file, 'wb' );
//
//        // split the string on commas
//        // $data[ 0 ] == "data:image/png;base64"
//        // $data[ 1 ] == <actual base64 string>
//        $data = explode( ',', $base64_string );
//
//        // we could add validation here with ensuring count( $data ) > 1
//        fwrite( $ifp, base64_decode( $data[ 1 ] ) );
//
//        // clean up the file resource
//        fclose( $ifp );
//
//        return $output_file;
//    }
//
//    #[Route('/partner', name: 'app_partner')]
//    public function index(): Response
//    {
//        header('Access-Control-Allow-Origin: *');
//        header('Access-Control-Allow-Methods: GET, POST');
//
//        return $this->json([
//           'status' => 200
//        ]);
//
////        return $this->render('partner/index.html.twig', [
////            'controller_name' => 'PartnerController',
////        ]);
//    }
}
