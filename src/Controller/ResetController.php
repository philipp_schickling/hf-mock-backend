<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use App\Repository\SavingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Uuid;

class ResetController extends AbstractController
{
    public function __construct(private CustomerRepository $customerRepository, private SavingRepository $savingRepository)
    {
    }

    #[Route('/clear_all/{apiKey}', name: 'clear_all')]
    public function clear_all(?Request $request, string $apiKey): Response
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');

        if('oI87abubaLka' !== $apiKey) {
            return $this->json(['Wrong API-Key']);
        }

        $this->customerRepository->deleteAll();
        $this->savingRepository->deleteAll();

        return $this->json(['All entries deleted.']);
    }

//    private function dropDb()
//    {
//        $application = new Application($this->kernel);
//        $application->setAutoExit(false);
//        $input = new ArrayInput([
//            'command' => 'doctrine:database:drop --force'
//        ]);
//        $output = new NullOutput();
//        $application->run($input, $output);
//    }
//
//    private function createDb()
//    {
//        $application = new Application($this->kernel);
//        $application->setAutoExit(false);
//        $input = new ArrayInput([
//            'command' => 'doctrine:database:create'
//        ]);
//        $output = new NullOutput();
//        $application->run($input, $output);
//    }
//
//    private function updateSchema()
//    {
//        $application = new Application($this->kernel);
//        $application->setAutoExit(false);
//        $input = new ArrayInput([
//            'command' => 'doctrine:schema:update'
//        ]);
//        $output = new NullOutput();
//        $application->run($input, $output);
//    }
}
