<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $number = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $company = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $city = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $postal_code = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $street = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?int $partner_step = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?float $data_quality = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $uses_transformer = false;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $last_action;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postal_code;
    }

    public function setPostalCode(?string $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getPartnerStep(): ?int
    {
        return $this->partner_step;
    }

    public function setPartnerStep(?int $partner_step): self
    {
        $this->partner_step = $partner_step;
        return $this;
    }

    public function getDataQuality(): ?float
    {
        return $this->data_quality;
    }

    public function setDataQuality(?float $data_quality): self
    {
        $this->data_quality = $data_quality;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function isUsesTransformer(): bool
    {
        return $this->uses_transformer;
    }

    public function setUsesTransformer(bool $uses_transformer): self
    {
        $this->uses_transformer = $uses_transformer;
        return $this;
    }

    public function getLastAction()
    {
        return $this->last_action;
    }

    public function setLastAction($last_action): self
    {
        $this->last_action = $last_action;
        return $this;
    }



}
