<?php

namespace App\Repository;

use App\Entity\Saving;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Saving>
 *
 * @method Saving|null find($id, $lockMode = null, $lockVersion = null)
 * @method Saving|null findOneBy(array $criteria, array $orderBy = null)
 * @method Saving[]    findAll()
 * @method Saving[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SavingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Saving::class);
    }

    public function add(Saving $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Saving $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * deletes all entities
     */
    public function deleteAll(): void
    {
        $builder = $this->createQueryBuilder('c');
        $builder->delete();

        $query = $builder->getQuery();
        $query->execute();
    }

//    public function findOneBySomeField($value): ?Saving
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
